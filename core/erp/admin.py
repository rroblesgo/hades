from django.contrib import admin
from .models import Employee, Type, Client, Product, Category, Sale, DetSale


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["name", "cate", "image", "pvp"]
    # list_filter = ['status', 'created', 'publish', 'author']
    # search_fields = ['title', 'body']
    # prepopulated_fields = {'slug': ('title',)}
    # raw_id_fields = ['author']
    # date_hierarchy = 'publish'
    # ordering = ['status', 'publish']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name"]


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ["names", "surnames", "dni", "birthday", "address", "sexo"]


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = ["cli", "date_joined", "subtotal", "iva", "total"]


@admin.register(DetSale)
class DetSaleAdmin(admin.ModelAdmin):
    list_display = ["sale", "prod", "price", "cant", "subtotal"]


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "dni",
        "date_joined",
        "type",
        "age",
        "salary",
        "state",
        "avatar",
        "cvitae",
    ]


@admin.register(Type)
class TypeAdmin(admin.ModelAdmin):
    list_display = ["name"]
