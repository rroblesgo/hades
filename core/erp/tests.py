import sys
from pathlib import Path

# sys.path.append("/Users/rrobles/Projects/DjangoProjects/Algorisoft/hades")
# sys.path.append(str(BASE_DIR))

BASE_DIR = Path(__file__).resolve().parent.parent.parent
sys.path = [str(BASE_DIR)] + sys.path

from config.wsgi import *
from core.erp.models import Type, Employee

# select datos
# query = Type.objects.all()
# print(query)

# insert datos
# try:
#     t = Type(name="Contable")
#     t.save()
#     query = Type.objects.all()
#     print(query)

# except Exception as e:
#     print(e)

# edición
# try:
#     t = Type.objects.get(id=55)
#     print(t)
#     query = Type.objects.all()
#     print(query)

# except Exception as e:
#     print(e)

# eliminación
# t = Type.objects.get(id=3)
# t.delete()
# query = Type.objects.all()
# print(query)

# select con filter
# obj = Type.objects.filter(name__endswith="a").exclude(id=2)
# print(obj.query)
# print(obj)

# for t in obj:
#     print(t.name)

# emp = Employee.objects.get(id=1)
# print(emp)
# print(emp.type.name)

# t = Type.objects.get(pk=1)
# print(t.name)
# ees = t.emples.all()
# for ee in ees:
#     print(ee.name)

# t2 = Type.objects.get(pk=1)
# print(t2.name)
# ees = t2.emples.all()
# print(ees)

# t2.delete()
print(BASE_DIR)
