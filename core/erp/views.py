from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .models import Category, Product


def myfirstview(request):
    data = {"title": "Categorías", "items": Category.objects.all()}
    return render(request, "home.html", data)


def mysecondview(request):
    data = {"title": "Productos", "items": Product.objects.all()}
    return render(request, "home.html", data)
