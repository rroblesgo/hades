from django.urls import path

from .views import myfirstview, mysecondview

app_name = "vistas"
urlpatterns = [
    path("categorias/", myfirstview, name="categorias"),
    path("productos/", mysecondview, name="productos"),
]
